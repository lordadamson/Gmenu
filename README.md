# Gmenu
Gmenu is a little class that lets you have a menu on your console application. Not the best code (I did it in my first year, but it works like a charm ;) :D )

## How to use this class:
The main function in this class is called `Gmenu();`
it takes up to 10 strings, prints them on the screen on the form of a menu, waits for the user to choose one of the strings
and returns a value that corresponds to the user's choice (from 0 to 9).
		
## Options this class provides:
- This class -by default- clears the screen when displaying a menu. However member function `clear_screen();` provides an option to clear or not
to clear the screen.
To clear the screen: `clear_screen(true);`
Not to clear the screen: `clear_screen(false);`

- This class provides an option to set the coordinates of the menu on the screen (it is (33, 5) by default) using `set_xy();` function. just sent the postion where you
want your menu to be. for example: `set_xy(20, 4);`

- The class provides an option to change the color of it's theme, using the function `set_color();` it takes one of five strings:
"red", "green", "turquoise", "yellow" and "purple".
for example: `set_color("red");`

## Sample Usage:
```
int main()
{
  Menu menu;
  int choice = menu.Gmenu("Play", "Settings", "Exit");
  switch (choice)
  {
    case 0: play();     break;
    case 1: Settings(); break;
    case 2: exit();     break;
  }
}
```

THAT'S ALL ENJOY YOUR CODING, THANK YOU :)
